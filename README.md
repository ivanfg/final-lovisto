# Final Lo Visto

Práctica final del curso 2020/21

# Entrega practica
## Datos
* Nombre: Iván Fernández García
* Titulación: Tecnologías de Telecomunicaciones
* Despliegue (url): https://ivanfg.pythonanywhere.com/LoVisto/
* Video básico (url):https://youtu.be/6x8nFuFZSoQ
* Video parte opcional (url):https://youtu.be/Tvd7gONsZ2Q

## Cuenta Admin Site
* usuario/contraseña admin/admin
## Cuentas usuarios
* usuario/contraseña ivanfg/futbol11
## Resumen parte obligatoria
* La aplicación gestiona tanto las aportaciones de los usuarios como los 
comentarios, valoraciones, etc.
* Creo que he realizado la funcionalidad mínima requerida, además también he añadido 
un par de recursos reconocidos ya que no he conseguido hacer el parser con youtube. 
Por lo demás creo que el funcionamiento de la práctica es el adecuado.
## Lista partes opcionales
* Nombre parte: favicon, url de imagen en comentario y 
 2 recursos reconocidos añadidos
 
 