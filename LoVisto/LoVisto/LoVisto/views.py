from django.shortcuts import render
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.template import loader
from django.contrib.auth import logout
from django.shortcuts import redirect, render
from . import models
from django.utils import timezone
from django.http import HttpResponse
from bs4 import BeautifulSoup
import requests
import requests_html
from urllib.request import urlopen
from django.http import JsonResponse

from .models import Contenido, Comentario, Voto


@csrf_exempt
def pagina_recurso(request, llave):
    modo = 0
    if request.method == "PUT":
        url_noticia = request.body.decode('utf-8')
    if request.method == "POST":
        action = request.POST['action']
        if action == "Editar contenido":
            try:
                contenido = Contenido.objects.get(titulo_noticia=llave)
                contenido.url_noticia = request.POST['url_noticia']
                contenido.save()
            except Contenido.DoesNotExist:
                contenido = Contenido(titulo_noticia=llave, url_noticia=request.POST['url_noticia'])
                contenido.save()

        elif action == "Enviar contenido":
            titulo_noticia = llave
            try:
                contenido = Contenido.objects.get(titulo_noticia=titulo_noticia)
                contenido.delete()
            except Contenido.DoesNotExist:
                pass
            contenido = Contenido(titulo_noticia=titulo_noticia, url_noticia=request.POST['url_noticia'],
                                  usuario=request.user.username,
                                  votos_positivos=0, votos_negativos=0, Num_comentarios=0, fecha=timezone.now())
            contenido.save()

        elif action == "LIKE":
            contenido = Contenido.objects.get(titulo_noticia=llave)
            contenido.votos_positivos += 1
            contenido.save()
            voto = Voto(contenido=Contenido.objects.get(titulo_noticia=llave), usuario=request.user.username,
                        valor="like")
            voto.save()

        elif action == "DISLIKE":
            contenido = Contenido.objects.get(titulo_noticia=llave)
            contenido.votos_negativos = contenido.votos_negativos + 1
            contenido.save()
            voto = Voto(contenido=Contenido.objects.get(titulo_noticia=llave), usuario=request.user.username,
                        valor="dislike")
            voto.save()

        elif action == "Inicio de sesion":
            return redirect('/login')

        elif action == "Cerrar sesion":
            logout(request)
            return redirect('/LoVisto')

        elif action == "Borrar contenido":
            try:
                contenido = Contenido.objects.get(titulo_noticia=llave)
                contenido.delete()
                return redirect('/LoVisto')
            except Contenido.DoesNotExist:
                contenido = Contenido.objects.get(titulo_noticia=llave, url_noticia=url_noticia)
                contenido.save()

        elif action == "Pagina principal":
            return redirect('/LoVisto')

        elif action == "Página usuario":
            return redirect('usuario/<request.user.username>')

        elif action == "Resumen aportaciones":
            return redirect('/LoVisto/aportaciones')

        elif action == "Info":
            return redirect('/LoVisto/info')
        elif action == "Oscuro":
            modo = 1
        elif action == "Claro":
            modo = 0
        elif action == "Borrar votacion":
            contenido = Contenido.objects.get(titulo_noticia=llave)
            voto = Voto.objects.get(contenido=Contenido.objects.get(titulo_noticia=llave),
                                    usuario=request.user.username)
            if voto.valor == "like":
                contenido.votos_positivos -= 1
            elif voto.valor == "dislike":
                contenido.votos_negativos -= 1
            contenido.save()
            voto.delete()

        elif action == "Enviar comentario":
            contenido = Contenido.objects.get(titulo_noticia=llave)
            contenido.Num_comentarios += 1
            coment = Comentario(contenido=contenido, titulo=request.POST['titulo'],
                                descripcion=request.POST['descripcion'],
                                url=request.POST['url'], fecha=timezone.now(), usuario=request.user.username)
            coment.save()
            contenido.save()
    try:
        Contenido.objects.get(titulo_noticia=llave)
    except Contenido.DoesNotExist:
        if request.GET.get("format") == "xml/":
            xml = loader.get_template('LoVisto/xml.html')
            contexto = {'Claves': Contenido.objects.all()}
            return HttpResponse(xml.render(contexto, request))
        if request.GET.get("format") == "json/":
           contenido = Contenido.objects.filter().values()
           return JsonResponse({"Contenido": list(contenido)})

        lista_contenidos = Contenido.objects.all()
        lista_contenidos_slice2 = lista_contenidos[0:3]
        contexto = {'lista_contenidos_slice2': lista_contenidos_slice2, 'modo': modo}
        return render(request, 'LoVisto/pagina_contenido_no_encontrado.html', contexto)

    lista_contenidos = Contenido.objects.all()
    lista_contenidos_slice2 = lista_contenidos[0:3]
    lista_comentarios = (Comentario.objects.all())
    lista_votos = Voto.objects.all()
    llave = llave

    contenido = Contenido.objects.get(titulo_noticia=llave)
    url = contenido.url_noticia
    headers = {'User-Agent': 'Mozilla/5.0'}
    try:
        html_page = requests.get(url, headers=headers)
        Bsoup = BeautifulSoup(html_page.text, 'html.parser')
        if "www.aemet.es" in url:
            try:
                title = Bsoup.find("title").text
                temperatura = Bsoup.find("div", class_="no_wrap").text
                fecha = Bsoup.find("th", class_="borde_izq_dcha_fecha").text
                maxmin = Bsoup.find("td", class_="alinear_texto_centro no_wrap comunes").text
                precipitacion = Bsoup.find("td", class_="nocomunes").text
                viento = Bsoup.find('div', class_="texto_viento").text

                contexto = {
                    'contenido': contenido,
                    'lista_contenidos_slice2': lista_contenidos_slice2,
                    'lista_comentarios': lista_comentarios,
                    'lista_votos': lista_votos,
                    'title': title,
                    'temperatura': temperatura,
                    'fecha': fecha,
                    'maxmin': maxmin,
                    'precipitacion': precipitacion,
                    'viento': viento,
                    'modo': modo
                }
                return render(request, 'LoVisto/pagina_AEMET.html', contexto)
            except AttributeError:
                contexto = {
                    'contenido': contenido,
                    'lista_contenidos_slice2': lista_contenidos_slice2,
                    'lista_comentarios': lista_comentarios,
                    'lista_votos': lista_votos,
                    'modo': modo
                }
            return render(request, 'LoVisto/pagina_recurso_error.html', contexto)

        elif "es.wikipedia.org" in url:
            try:
                title = Bsoup.find(id="firstHeading").text
                text = Bsoup.find(id="bodyContent").text
                txt = text[0:400]
                images = Bsoup.findAll('img')
                imagen = ((images[0])['src'])

                contexto = {
                    'contenido': contenido,
                    'lista_contenidos_slice2': lista_contenidos_slice2,
                    'lista_comentarios': lista_comentarios,
                    'lista_votos': lista_votos,
                    'title': title,
                    'txt': txt,
                    'imagen': imagen,
                    'modo': modo
                }
                return render(request, 'LoVisto/pagina_wiki.html', contexto)
            except AttributeError:
                contexto = {
                    'contenido': contenido,
                    'lista_contenidos_slice2': lista_contenidos_slice2,
                    'lista_comentarios': lista_comentarios,
                    'lista_votos': lista_votos,
                    'modo': modo}
            return render(request, 'LoVisto/pagina_recurso_error.html', contexto)

        elif "www.reddit.com" in url:
            try:
                text = Bsoup.find("p", class_="_1qeIAgB0cPwnLhDF9XSiJM").text
                title = Bsoup.find("title").text
                images = Bsoup.findAll('img')
                imagen = ((images[0])['src'])

                contexto = {
                    'contenido': contenido,
                    'lista_contenidos_slice2': lista_contenidos_slice2,
                    'lista_comentarios': lista_comentarios,
                    'lista_votos': lista_votos,
                    'text': text,
                    'title': title,
                    'imagen': imagen,
                    'modo': modo}
                return render(request, 'LoVisto/pagina_reddit.html', contexto)
            except AttributeError:
                contexto = {
                    'contenido': contenido,
                    'lista_contenidos_slice2': lista_contenidos_slice2,
                    'lista_comentaarios': lista_comentarios,
                    'lista_votos': lista_votos,
                    'modo': modo
                }
            return render(request, 'LoVisto/pagina_recurso_error.html', contexto)


        elif "www.marca.com" in url:
            try:
                title = Bsoup.find(class_="ue-c-article__headline js-headline js-headlineb").text
                text = Bsoup.find("p", class_="ue-c-article__standfirst").text
                images = Bsoup.findAll('img')
                imagen = ((images[1])['src'])
                contexto = {
                    'contenido': contenido,
                    'lista_contenidos_slice2': lista_contenidos_slice2,
                    'lista_comentarios': lista_comentarios,
                    'lista_votos': lista_votos,
                    'title': title,
                    'text': text,
                    'imagen': imagen,
                    'modo': modo
                }
                return render(request, 'LoVisto/pagina_marca.html', contexto)
            except AttributeError:
                contexto = {
                    'contenido': contenido,
                    'lista_contenidos_slice2': lista_contenidos_slice2,
                    'lista_comentarios': lista_comentarios,
                    'lista_votos': lista_votos,
                    'modo': modo
                }
            return render(request, 'LoVisto/pagina_recurso_error.html', contexto)

        elif "www.larazon.es" in url:
            try:
                title = Bsoup.find("h1").text
                text = Bsoup.find("h2").text
                images = Bsoup.findAll('img')
                imagen = ((images[0])['src'])
                contexto = {
                    'contenido': contenido,
                    'lista_contenidos_slice2': lista_contenidos_slice2,
                    'lista_comentarios': lista_comentarios,
                    'lista_votos': lista_votos,
                    'title': title,
                    'text': text,
                    'imagen': imagen,
                    'modo': modo
                }
                return render(request, 'LoVisto/pagina_larazon.html', contexto)
            except AttributeError:
                contexto = {
                    'contenido': contenido,
                    'lista_contenidos_slice2': lista_contenidos_slice2,
                    'lista_comentarios': lista_comentarios,
                    'lista_votos': lista_votos,
                    'modo': modo
                }
            return render(request, 'LoVisto/pagina_recurso_error.html', contexto)

        else:
            try:
                images = Bsoup.findAll('img')
                title = Bsoup.find("title").text
                imagen = ((images[1])['src'])
                contexto = {
                    'contenido': contenido,
                    'lista_contenidos_slice2': lista_contenidos_slice2,
                    'lista_comentarios': lista_comentarios,
                    'lista_votos': lista_votos,
                    'title': title,
                    'imagen': imagen,
                    'modo': modo
                }
                return render(request, 'LoVisto/pagina_recurso_no_rec.html', contexto)
            except AttributeError:
                contexto = {
                    'contenido': contenido,
                    'lista_contenidos_slice2': lista_contenidos_slice2,
                    'lista_comentarios': lista_comentarios,
                    'lista_votos': lista_votos,
                    'modo': modo
                }
            return render(request, 'LoVisto/pagina_recurso_error.html', contexto)
    except:
        contexto = {
            'contenido': contenido,
            'lista_contenidos_slice2': lista_contenidos_slice2,
            'lista_comentarios': lista_comentarios,
            'lista_votos': lista_votos,
            'modo': modo
        }
    return render(request, 'LoVisto/pagina_recurso_error.html', contexto)


@csrf_exempt
def index(request):
    modo = 0
    if request.method == "POST":
        action = request.POST['action']
        if action == "Añadir contenido":
            try:
                contenido = Contenido.objects.get(titulo_noticia=request.POST['titulo_noticia'])
                contenido.delete()
            except Contenido.DoesNotExist:
                contenido = Contenido(titulo_noticia=request.POST['titulo_noticia'],
                                      url_noticia=request.POST['url_noticia'],
                                      usuario=request.user.username,
                                      votos_positivos=0,
                                      votos_negativos=0,
                                      Num_comentarios=0,
                                      fecha=timezone.now())
                contenido.save()
        elif action == "Inicio de sesion":
            return redirect('/login')
        elif action == "Cerrar sesion":
            logout(request)
            return redirect('/LoVisto')
        elif action == "Info":
            return redirect('/LoVisto/info')
        elif action == "Resumen aportaciones":
            return redirect('/LoVisto/aportaciones')
        elif action == "Oscuro":
            modo = 1
        elif action == "Claro":
            modo = 0

        elif action == "Página usuario":
            return redirect('usuario/' + request.user.username)

    lista_contenidos = Contenido.objects.all()
    lista_contenidos = list(reversed(lista_contenidos))
    lista_contenidos_slice = lista_contenidos[0:10]
    lista_contenidos_slice2 = lista_contenidos[0:3]
    aportaciones = 10
    iterador = 0
    contexto = {
        'lista_contenidos_slice': lista_contenidos_slice,
        'lista_contenidos': lista_contenidos,
        'aportaciones': aportaciones,
        'iterador': iterador,
        'lista_contenidos_slice2': lista_contenidos_slice2,
        'modo': modo
    }
    return render(request, 'LoVisto/index.html', contexto)


def pagina_usuario(request, llave):
    modo = 0
    if request.method == "POST":
        action = request.POST['action']
        if request.POST['action'] == "Pagina principal":
            return redirect('/LoVisto')
        elif action == "Cerrar sesion":
            logout(request)
            return redirect('/LoVisto')
        elif action == "Resumen aportaciones":
            return redirect('/LoVisto/aportaciones')
        elif action == "Info":
            return redirect('/LoVisto/info')
        elif action == "Oscuro":
            modo = 1
        elif action == "Claro":
            modo = 0
    lista_contenidos = Contenido.objects.all()
    lista_contenidos_slice2 = lista_contenidos[0:3]
    lista_comentarios = Comentario.objects.all()
    lista_votos = Voto.objects.all()
    contexto = {
        'lista_contenidos': lista_contenidos,
        'lista_comentarios': lista_comentarios,
        'lista_votos': lista_votos,
        'lista_contenidos_slice2': lista_contenidos_slice2,
        'modo': modo
    }
    return render(request, 'LoVisto/pagina_usuario.html', contexto)


def pagina_aportaciones(request):
    modo = 0
    if request.method == "POST":
        action = request.POST['action']
        if request.POST['action'] == "Pagina principal":
            return redirect('/LoVisto')
        elif action == "Cerrar sesion":
            logout(request)
            return redirect('/LoVisto')
        elif action == "Inicio de sesion":
            return redirect('/login')
        elif action == "Página usuario":
            return redirect('usuario/' + request.user.username)
        elif action == "Info":
            return redirect('/LoVisto/info')
        elif action == "Oscuro":
            modo = 1
        elif action == "Claro":
            modo = 0
    lista_contenidos = list(reversed(Contenido.objects.all()))
    lista_contenidos_slice2 = lista_contenidos[0:3]

    contexto = {
        'lista_contenidos': lista_contenidos,
        'lista_contenidos_slice2': lista_contenidos_slice2,
        'modo': modo
    }
    return render(request, 'LoVisto/pagina_aportaciones.html', contexto)


def pagina_info(request):
    modo = 0
    if request.method == "POST":
        action = request.POST['action']
        if request.POST['action'] == "Pagina principal":
            return redirect('/LoVisto')
        elif action == "Cerrar sesion":
            logout(request)
            return redirect('/LoVisto')
        elif action == "Inicio de sesion":
            return redirect('/login')
        elif action == "Página usuario":
            return redirect('usuario/' + request.user.username)
        elif action == "Info":
            return redirect('/LoVisto/info')
        elif action == "Resumen aportaciones":
            return redirect('/LoVisto/aportaciones')
        elif action == "Oscuro":
            modo = 1
        elif action == "Claro":
            modo = 0
    lista_contenidos = list(reversed(Contenido.objects.all()))
    lista_contenidos_slice2 = lista_contenidos[0:3]
    contexto = {
        'lista_contenidos_slice2': lista_contenidos_slice2,
        'modo': modo
    }
    return render(request, 'LoVisto/pagina_info.html', contexto)

