from django.db import models

# Create your models here.

from django.db import models
from django.utils import timezone

# Create your models here.

class Topic(models.Model):
    TOPICS = [('AE','Aemet'), ('WK','Wikipedia'), ('YU','Youtube'), ('RE','Reddit'),('OT','Other')]
    topic = models.CharField(max_length=2, choices=TOPICS)
    def __str__(self):
       return str(self.id) + ": " + self.topic


class Contenido(models.Model):
    usuario = models.TextField()
    titulo_noticia = models.CharField(max_length=128) #clave
    url_noticia = models.TextField() #valor
    Num_comentarios = models.IntegerField()
    votos_positivos = models.IntegerField()
    votos_negativos = models.IntegerField()
    fecha = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return str(self.id) + ": " + self.titulo_noticia



class Comentario(models.Model):
    usuario = models.TextField()
    contenido = models.ForeignKey(Contenido, on_delete=models.CASCADE)
    descripcion = models.TextField()
    url = models.TextField(default="No url")
    titulo = models.CharField(max_length=64, default="No title")
    fecha = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return str(self.id) + ": " + self.usuario

class Voto(models.Model):
    usuario = models.TextField()
    contenido = models.ForeignKey(Contenido, on_delete=models.CASCADE)
    valor = models.TextField(default="No valor")

    def __str__(self):
        return str(self.id) + ": " + self.usuario

